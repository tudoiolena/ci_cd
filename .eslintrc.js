module.exports = {
  env: {
    node: true,
    jest: true,
  },
  globals: {
    jest: true,
    test: true,
  },
  extends: ["eslint:recommended", "plugin:@typescript-eslint/recommended"],
  parser: "@typescript-eslint/parser",
  parserOptions: {
    project: "tsconfig.json",
    ecmaVersion: "latest",
    sourceType: "module",
  },
  plugins: ["@typescript-eslint/eslint-plugin", "jest"],
  root: true,
  ignorePatterns: [".eslintrc.js"],
  rules: {},
};
