import request from "supertest";
import express, { Express } from "express";
import { getTimeStamp } from "../src/utils";

describe("Index Route", () => {
  let app: Express;
  const port: number = 8002;
  let server: any;

  beforeAll(async () => {
    app = express();
    app.get("/", async (req, res) => {
      try {
        const timestamp = await getTimeStamp();
        console.log(`Received a request at ${timestamp}`);
        res.send(`Express + Typescript Server. Timestamp: ${timestamp}`);
      } catch (error) {
        console.error("Error while getting timestamp:", error);
        res.status(500).send("An error occurred.");
      }
    });

    server = await app.listen(port);
    console.log(`[Test Server]: Server is running at http://localhost:${port}`);
  });

  afterAll(async () => {
    await server.close();
    console.log("[Test Server]: Server closed");
  });

  it("returns a response with status 200", async () => {
    const response = await request(app).get("/");
    expect(response.status).toEqual(200);
  });

  it("returns 'Express + Typescript Server' message", async () => {
    const response = await request(app).get("/");
    const expectedResponse = "Express + Typescript Server. Timestamp:";
    expect(response.text).toContain(expectedResponse);
  });
});
