FROM node:18.16.0-alpine3.17
WORKDIR /app
COPY . /app
RUN npm install
RUN npm run build
EXPOSE 8001
CMD ["npm", "start"]