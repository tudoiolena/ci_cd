"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.app = void 0;
const express_1 = __importDefault(require("express"));
const dotenv_1 = __importDefault(require("dotenv"));
const utils_1 = require("./utils");
dotenv_1.default.config();
exports.app = (0, express_1.default)();
const port = process.env.PORT;
exports.app.get("/", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const timestamp = yield (0, utils_1.getTimeStamp)();
        console.log(`Received a request at ${timestamp}`);
        res.send(`Express + Typescript Server. Timestamp: ${timestamp}`);
    }
    catch (error) {
        console.error("Error while getting timestamp:", error);
        res.status(500).send("An error occurred.");
    }
}));
exports.app.listen(port, () => {
    console.log(`[Server]: Server is running at http://localhost:${port}`);
});
