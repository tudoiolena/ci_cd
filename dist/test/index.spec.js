"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const supertest_1 = __importDefault(require("supertest"));
const express_1 = __importDefault(require("express"));
const utils_1 = require("../src/utils");
describe("Index Route", () => {
    let app;
    const port = 8002;
    let server;
    beforeAll(() => __awaiter(void 0, void 0, void 0, function* () {
        app = (0, express_1.default)();
        app.get("/", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
            try {
                const timestamp = yield (0, utils_1.getTimeStamp)();
                console.log(`Received a request at ${timestamp}`);
                res.send(`Express + Typescript Server. Timestamp: ${timestamp}`);
            }
            catch (error) {
                console.error("Error while getting timestamp:", error);
                res.status(500).send("An error occurred.");
            }
        }));
        server = yield app.listen(port);
        console.log(`[Test Server]: Server is running at http://localhost:${port}`);
    }));
    afterAll(() => __awaiter(void 0, void 0, void 0, function* () {
        yield server.close();
        console.log("[Test Server]: Server closed");
    }));
    it("returns a response with status 200", () => __awaiter(void 0, void 0, void 0, function* () {
        const response = yield (0, supertest_1.default)(app).get("/");
        expect(response.status).toEqual(200);
    }));
    it("returns 'Express + Typescript Server' message", () => __awaiter(void 0, void 0, void 0, function* () {
        const response = yield (0, supertest_1.default)(app).get("/");
        const expectedResponse = "Express + Typescript Server. Timestamp:";
        expect(response.text).toContain(expectedResponse);
    }));
});
