import express, { Express, Request, Response } from "express";
import dotenv from "dotenv";
import { getTimeStamp } from "./utils";

dotenv.config();

export const app: Express = express();
const port = process.env.PORT;

app.get("/", async (req: Request, res: Response) => {
  try {
    const timestamp = await getTimeStamp();
    console.log(`Received a request at ${timestamp}`);
    res.send(`Express + Typescript Server. Timestamp: ${timestamp}`);
  } catch (error) {
    console.error("Error while getting timestamp:", error);
    res.status(500).send("An error occurred.");
  }
});

app.listen(port, () => {
  console.log(`[Server]: Server is running at http://localhost:${port}`);
});
