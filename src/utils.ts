export async function getTimeStamp(): Promise<string> {
  return new Promise((resolve) => {
    const timestamp = new Date().getTime();
    resolve(timestamp.toString());
  });
}
